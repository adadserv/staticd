package app

import (
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"os"
	"os/signal"
	"syscall"
)

func Run() {
	config, err := ReadConfig("")
	if err != nil {
		log.Fatal().Err(err).Msg("read config failed")
	}

	assets, err := config.Assets()
	if err != nil {
		log.Fatal().Err(err).Msg("get assets failed")
	}

	cancel := make(chan os.Signal, 2)

	signal.Notify(cancel, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-cancel
		os.Exit(1)
	}()

	addr := config.Address()
	handler := New(config, assets...)

	log.Info().Msgf("listen on %s", addr)

	if err := fasthttp.ListenAndServe(addr, handler.Handle); err != nil {
		log.Fatal().Err(err)
	}
}
