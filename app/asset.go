package app

type Header struct {
	Name  []byte
	Value []byte
}

type Asset struct {
	Path string

	Length       int
	LengthGzip   int
	ETag         []byte
	LastModified []byte
	ContentType  []byte

	Data    []byte
	Gzipped []byte
	//Headers    []Header
}
