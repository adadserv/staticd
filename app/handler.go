package app

import (
	"bytes"
	"github.com/oklog/ulid"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"math/rand"
	"time"
)

var (
	acceptGzip   = []byte("gzip")
	cacheControl = []byte("public, max-age=3600")

	headerContentEncoding = []byte(fasthttp.HeaderContentEncoding)
	headerETag            = []byte(fasthttp.HeaderETag)
	headerLastModified    = []byte(fasthttp.HeaderLastModified)
	headerCacheControl    = []byte(fasthttp.HeaderCacheControl)
	headerExpires         = []byte(fasthttp.HeaderExpires)
)

type Handler struct {
	assets  map[string]Asset
	expires []byte

	targetName   []byte
	targetDomain []byte
	targetPath   []byte
	targetExpire time.Time
	targetSecure bool
	cookieMaxAge int
}

func New(config *Config, assets ...Asset) *Handler {
	h := &Handler{
		assets:  make(map[string]Asset, len(assets)),
		expires: []byte(time.Now().Add(time.Hour).Format(time.RFC1123)),

		targetName:   []byte(config.TargetName),
		targetDomain: []byte(config.TargetDomain),
		targetPath:   []byte(config.TargetPath),
		cookieMaxAge: config.CookieMaxAge,
		targetSecure: config.TargetSecure,
		targetExpire: time.Now().Add(time.Duration(config.CookieMaxAge) * time.Second),
	}

	for _, asset := range assets {
		h.assets[asset.Path] = asset
	}

	go func() {
		ticker := time.Tick(time.Minute)

		for range ticker {
			h.expires = []byte(time.Now().Add(time.Hour).Format(time.RFC1123))
		}
	}()

	return h
}

func (Handler) UID() (string, error) {
	t := time.Now()
	et := rand.New(rand.NewSource(t.UnixNano()))

	uid, err := ulid.New(ulid.Timestamp(t), et)
	if err != nil {
		return "", errors.Wrap(err, "creating id failed")
	}

	return uid.String(), nil
}

func (h Handler) FlashUID(uid string) {
	time.Sleep(50 * time.Millisecond)
}

func (h Handler) Handle(ctx *fasthttp.RequestCtx) {
	path := string(ctx.Path()[1:])

	asset, exists := h.assets[path]
	if !exists {
		log.Error().Msgf("not found %s", path)
		ctx.SetStatusCode(fasthttp.StatusNotFound)

		return
	}

	var (
		uid string
		err error
	)

	if cb := ctx.Request.Header.CookieBytes(h.targetName); cb == nil {
		uid, err = h.UID()
		if err != nil {
			log.Error().Err(err).Msg("create uid failed")
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)

			return
		}

		go h.FlashUID(uid)
	} else {
		uid = string(cb)
	}

	acceptEncoding := ctx.Request.Header.Peek(fasthttp.HeaderAcceptEncoding)
	data := asset.Data

	if !bytes.Contains(acceptEncoding, acceptGzip) {
		ctx.Response.Header.SetBytesKV(headerContentEncoding, acceptGzip)
		ctx.Response.Header.SetContentLength(asset.LengthGzip)
		data = asset.Gzipped
	} else {
		ctx.Response.Header.SetContentLength(asset.Length)
	}

	ctx.Response.Header.SetBytesKV(headerETag, asset.ETag)
	ctx.Response.Header.SetBytesKV(headerLastModified, asset.LastModified)
	ctx.Response.Header.SetBytesKV(headerCacheControl, cacheControl)
	ctx.Response.Header.SetBytesKV(headerExpires, h.expires)
	ctx.Response.Header.SetContentTypeBytes(asset.ContentType)

	cookie := &fasthttp.Cookie{}
	cookie.SetKeyBytes(h.targetName)
	cookie.SetValue(uid)
	cookie.SetDomainBytes(h.targetDomain)
	cookie.SetMaxAge(h.cookieMaxAge)
	cookie.SetPathBytes(h.targetPath)
	cookie.SetExpire(h.targetExpire)
	cookie.SetSecure(h.targetSecure)
	ctx.Response.Header.SetCookie(cookie)

	_, err = ctx.Write(data)
	if err != nil {
		status := fasthttp.StatusInternalServerError
		log.Error().Int("status", status).Err(err).Msg("write body failed")
		ctx.SetStatusCode(status)
	}
}
