module gitlab.com/adadserv/staticd

go 1.13

require (
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/gabriel-vasile/mimetype v1.0.2
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/klauspost/compress v1.10.1 // indirect
	github.com/labstack/echo/v4 v4.1.14
	github.com/oklog/ulid v1.3.1
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.18.0
	github.com/valyala/fasthttp v1.9.0
)
