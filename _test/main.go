package main

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/hex"
	"github.com/dimfeld/httptreemux"
	"github.com/go-chi/chi"
	"github.com/gorilla/mux"
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	testFastHttp()
	//testHttp()
	//testEcho()
	//testChi()
	//testMux()
	//testHttpTree()
}

func testHttpTree() {
	b, info := getFile()

	length := strconv.Itoa(len(b))
	hash := md5.Sum(b)

	eTag := hex.EncodeToString(hash[:])
	lastModified := info.ModTime().Format(time.RFC850)
	contentType := "application/javascript"
	cacheControl := "public, max-age=3600"

	r := httptreemux.New()
	r.GET("/init.js", func(w http.ResponseWriter, r *http.Request, _ map[string]string) {
		status := http.StatusOK

		w.Header().Set(fasthttp.HeaderContentEncoding, "gzip")
		w.Header().Set(fasthttp.HeaderLastModified, lastModified)
		w.Header().Set(fasthttp.HeaderCacheControl, cacheControl)
		w.Header().Set(fasthttp.HeaderContentType, contentType)
		w.Header().Set(fasthttp.HeaderETag, eTag)
		w.Header().Set(fasthttp.HeaderContentLength, length)
		w.Header().Set(fasthttp.HeaderExpires, time.Now().Format(time.RFC850))
		w.WriteHeader(status)

		_, err := w.Write(b)
		if err != nil {
			status = http.StatusInternalServerError
			log.Error().Int("status", status).Err(err).Msg("write body failed")
		}
	})

	log.Info().Msg("start httptreemux on :8092")
	log.Fatal().Err(http.ListenAndServe(":8092", r))
}

func testMux() {
	b, info := getFile()

	length := strconv.Itoa(len(b))
	hash := md5.Sum(b)

	eTag := hex.EncodeToString(hash[:])
	lastModified := info.ModTime().Format(time.RFC850)
	contentType := "application/javascript"
	cacheControl := "public, max-age=3600"

	r := mux.NewRouter()
	r.HandleFunc("/init.js", func(w http.ResponseWriter, r *http.Request) {
		status := http.StatusOK

		w.Header().Set(fasthttp.HeaderContentEncoding, "gzip")
		w.Header().Set(fasthttp.HeaderLastModified, lastModified)
		w.Header().Set(fasthttp.HeaderCacheControl, cacheControl)
		w.Header().Set(fasthttp.HeaderContentType, contentType)
		w.Header().Set(fasthttp.HeaderETag, eTag)
		w.Header().Set(fasthttp.HeaderContentLength, length)
		w.Header().Set(fasthttp.HeaderExpires, time.Now().Format(time.RFC850))
		w.WriteHeader(status)

		_, err := w.Write(b)
		if err != nil {
			status = http.StatusInternalServerError
			log.Error().Int("status", status).Err(err).Msg("write body failed")
		}
	})

	log.Info().Msg("start mux on :8092")
	log.Fatal().Err(http.ListenAndServe(":8092", r))
}

func testChi() {
	b, info := getFile()

	length := strconv.Itoa(len(b))
	hash := md5.Sum(b)

	eTag := hex.EncodeToString(hash[:])
	lastModified := info.ModTime().Format(time.RFC850)
	contentType := "application/javascript"
	cacheControl := "public, max-age=3600"

	r := chi.NewMux()
	r.Get("/init.js", func(w http.ResponseWriter, r *http.Request) {
		status := http.StatusOK

		w.Header().Set(fasthttp.HeaderContentEncoding, "gzip")
		w.Header().Set(fasthttp.HeaderLastModified, lastModified)
		w.Header().Set(fasthttp.HeaderCacheControl, cacheControl)
		w.Header().Set(fasthttp.HeaderContentType, contentType)
		w.Header().Set(fasthttp.HeaderETag, eTag)
		w.Header().Set(fasthttp.HeaderContentLength, length)
		w.Header().Set(fasthttp.HeaderExpires, time.Now().Format(time.RFC850))
		w.WriteHeader(status)

		_, err := w.Write(b)
		if err != nil {
			status = http.StatusInternalServerError
			log.Error().Int("status", status).Err(err).Msg("write body failed")
		}
	})

	log.Info().Msg("start chi on :8092")
	log.Fatal().Err(http.ListenAndServe(":8092", r))
}

func testEcho() {
	b, info := getFile()

	length := strconv.Itoa(len(b))
	hash := md5.Sum(b)

	eTag := hex.EncodeToString(hash[:])
	lastModified := info.ModTime().Format(time.RFC850)
	contentType := "application/javascript"
	cacheControl := "public, max-age=3600"

	e := echo.New()

	e.GET("/init.js", func(ctx echo.Context) error {
		ctx.Response().Header().Set(fasthttp.HeaderContentEncoding, "gzip")
		ctx.Response().Header().Set(fasthttp.HeaderLastModified, lastModified)
		ctx.Response().Header().Set(fasthttp.HeaderCacheControl, cacheControl)
		//ctx.Response().Header().Set(fasthttp.HeaderContentType, contentType)
		ctx.Response().Header().Set(fasthttp.HeaderETag, eTag)
		ctx.Response().Header().Set(fasthttp.HeaderContentLength, length)
		ctx.Response().Header().Set(fasthttp.HeaderExpires, time.Now().Format(time.RFC850))

		return ctx.Blob(http.StatusOK, contentType, b)
	})

	log.Info().Msg("start echo on :8092")
	log.Fatal().Err(e.Start(":8092"))
}

func getFile() ([]byte, os.FileInfo) {
	info, err := os.Stat("./assets/init.js")
	if err != nil {
		log.Fatal().Err(err)
	}

	//info, err := f.Stat()
	//if err != nil {
	//	log.Fatal().Err(err)
	//}

	b, err := ioutil.ReadFile("./assets/init.js")
	if err != nil {
		log.Fatal().Err(err)
	}

	buf := &bytes.Buffer{}
	w := gzip.NewWriter(buf)

	_, err = w.Write(b)
	if err != nil {
		log.Fatal().Err(err)
	}

	return buf.Bytes(), info
}

func testHttp() {
	b, info := getFile()

	length := strconv.Itoa(len(b))
	hash := md5.Sum(b)

	eTag := hex.EncodeToString(hash[:])
	lastModified := info.ModTime().Format(time.RFC850)
	contentType := "application/javascript"
	cacheControl := "public, max-age=3600"

	h := &http.ServeMux{}
	h.HandleFunc("/init.js", func(w http.ResponseWriter, r *http.Request) {
		status := http.StatusOK

		w.Header().Set(fasthttp.HeaderContentEncoding, "gzip")
		w.Header().Set(fasthttp.HeaderLastModified, lastModified)
		w.Header().Set(fasthttp.HeaderCacheControl, cacheControl)
		w.Header().Set(fasthttp.HeaderContentType, contentType)
		w.Header().Set(fasthttp.HeaderETag, eTag)
		w.Header().Set(fasthttp.HeaderContentLength, length)
		w.Header().Set(fasthttp.HeaderExpires, time.Now().Format(time.RFC850))
		w.WriteHeader(status)

		_, err := w.Write(b)
		if err != nil {
			status = http.StatusInternalServerError
			log.Error().Int("status", status).Err(err).Msg("write body failed")
		}
	})

	log.Info().Msg("start http on :8092")
	log.Fatal().Err(http.ListenAndServe(":8092", h))
}

func testFastHttp() {
	b, info := getFile()

	length := len(b)
	hash := md5.Sum(b)

	hETag := []byte(fasthttp.HeaderETag)
	eTag := []byte(hex.EncodeToString(hash[:]))

	hLastModified := []byte(fasthttp.HeaderLastModified)
	lastModified := []byte(info.ModTime().Format(time.RFC850))
	contentType := []byte("application/javascript")
	hCacheControl := []byte(fasthttp.HeaderCacheControl)
	cacheControl := []byte("public, max-age=3600")
	hContentEncoding := []byte(fasthttp.HeaderContentEncoding)
	contentEncoding := []byte("gzip")
	hExpires := []byte(fasthttp.HeaderExpires)

	h := func(ctx *fasthttp.RequestCtx) {
		switch string(ctx.Path()) {
		case "/init.js":
			status := fasthttp.StatusOK

			ctx.Response.Header.SetBytesKV(hETag, eTag)
			ctx.Response.Header.SetBytesKV(hLastModified, lastModified)
			ctx.Response.Header.SetContentLength(length)
			ctx.Response.Header.SetContentTypeBytes(contentType)
			ctx.Response.Header.SetBytesKV(hCacheControl, cacheControl)
			ctx.Response.Header.SetBytesK(hExpires, time.Now().Add(time.Hour).Format(time.RFC1123))
			ctx.Response.Header.SetBytesKV(hContentEncoding, contentEncoding)
			ctx.SetStatusCode(status)

			_, err := ctx.Write(b)
			if err != nil {
				status = fasthttp.StatusInternalServerError
				log.Error().Int("status", status).Err(err).Msg("write body failed")
			}

			return
		default:
			log.Error().Int("status", fasthttp.StatusNotFound).Msg("not found")
			ctx.Error("not found", fasthttp.StatusNotFound)
		}
	}

	log.Info().Msg("start fasthttp on :8092")
	log.Fatal().Err(fasthttp.ListenAndServe(":8092", h))
}
